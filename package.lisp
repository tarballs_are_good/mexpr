;;;; package.lisp
;;;; (c) 2011 Robert Smith

;;;; Declare the mexpr package.

(defpackage #:mexpr
  (:use :cl :cl-lex :yacc)
  (:export :mread-string))
