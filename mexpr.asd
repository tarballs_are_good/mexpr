(defpackage #:mexpr-asd
  (:use :cl :asdf))

(in-package :mexpr-asd)

(defsystem mexpr
  :name "mexpr"
  :version "0.0.1"
  :maintainer "Robert Smith"
  :author "Robert Smith"
  :license "MIT license"
  :description "Pseudo M-expression parser."
  :long-description "A parser for pseudo-M-expressions. These
  M-expressions parse into S-expressions for further lisp processing."
  :depends-on (:cl-lex :yacc)
  :serial t
  :components ((:file "package")

               ;;load last:
               (:file "mexpr")
               ))
