;;;; mexpr.lisp

(in-package :mexpr)

;;; The lexer

(define-condition lexer-error (yacc-runtime-error)
  ((character :initarg :character :reader lexer-error-character))
  (:report (lambda (e stream)
             (format stream "Lexing failed~@[: unexpected character ~S~]"
                     (lexer-error-character e)))))

(defun lexer-error (char)
  (error (make-condition 'lexer-error :character char)))

(defun maybe-unread (char stream)
  (when char
    (unread-char char stream)))

(defun read-number (stream)
  (let ((v nil))
    (loop
       (let ((c (read-char stream nil nil)))
         (when (or (null c) (not (digit-char-p c)))
           (maybe-unread c stream)
           (when (null v)
             (lexer-error c))
           (return-from read-number v))
         (setf v (+ (* (or v 0) 10)
                    (- (char-code c) (char-code #\0))))))))

(defun intern-id (string)
  (read-from-string string)
  #+#:ignore
  (let ((*package* (find-package :keyword)))
    (read-from-string string)))

(defun read-id (stream)
  (let ((v '()))
    (loop
       (let ((c (read-char stream nil nil)))
         (when (or (null c)
                   (not (or (digit-char-p c)
                            (alpha-char-p c)
                            (eql c #\_))))
           (maybe-unread c stream)
           (when (null v)
             (lexer-error c))
           (return-from read-id (intern-id (coerce (nreverse v) 'string))))
         (push c v)))))

(defun lexer (&optional (stream *standard-input*))
  (loop
     (let ((c (read-char stream nil nil)))
       (cond
         ;; Whitespace
         ((member c '(#\Space #\Tab))
          ;; Do nothing
          )

         ;; Newlines
         ((member c '(nil #\Newline))
          (return-from lexer (values nil nil)))
         
         ;; Mixfix
         ((member c '(#\( #\) #\[ #\]))
          (let ((symbol (intern (string c) (find-package :keyword))))
            (return-from lexer (values symbol symbol))))
         
         ;; Infix
         ((member c '(#\+ #\- #\* #\/ #\, #\^ #\= #\> #\<))
          (let ((symbol (intern (string c) (find-package :keyword))))
            (return-from lexer (values symbol symbol))))
         
         ;; Numbers
         ((digit-char-p c)
          (unread-char c stream)
          (return-from lexer (values 'int (read-number stream))))
         
         ;; Identifiers
         ((alpha-char-p c)
          (unread-char c stream)
          (return-from lexer (values 'id (read-id stream))))
         
         ;; Unknown... error
         (t
          (lexer-error c))))))

;;; The parser

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun i2p (a b c)
    "Infix to prefix"
    (list b a c))

  (defun f2p (a b c d)
    "Function to prefix."
    (declare (ignore b d))
    (cons a c))

  (defun nullary-f2p (a b c)
    "Nullary function to prefix."
    (declare (ignore b c))
    (list a))

  (defun l2p (a b c)
    "List expression to list."
    (declare (ignore b))
    (cons a c))

  (defun e2p (a b c)
    "List expression to list."
    (cons a (cons b c)))

  (defun k-1-3 (a b c)
    "First out of three"
    (declare (ignore b c))
    a)

  (defun k-2-3 (a b c)
    "Second out of three"
    (declare (ignore a c))
    b)
)

(define-parser *expression-parser*
  (:start-symbol expr-opt)
  (:terminals (int id :+ :- :* :/ :^ := :> :<
                   :|,|
                   :|(| :|)|
                   :|[| :|]|
                   ))
  (:precedence ((:right :^)
                (:left :* :/)
                (:left :+ :-)
                (:nonassoc :|,| := :> :<)
                ))

  ;; optional expr
  (expr-opt
   expr
   ())

  (expr
   (expr :^ expr #'i2p)
   (expr :+ expr #'i2p)
   (expr :- expr #'i2p)
   (expr :* expr #'i2p)
   (expr :/ expr #'i2p)
   (expr := expr #'i2p)
   (expr :> expr #'i2p)
   (expr :< expr #'i2p)
;;   compound
   funarg
   )

#|
  (expr-semicolon-list
   (expr)
   (expr :|;| expr-semicolon-list #'l2p)
   )

  (compound
   (compound :|;| #'nullary-f2p)
   (compound :|[| expr-comma-list :|]| #'f2p)
   term
   )
|#

  (expr-comma-list
   (expr)
   (expr :|,| expr-comma-list #'l2p)
   )

  (funarg
   (funarg :|[| :|]| #'nullary-f2p)
   (funarg :|[| expr-comma-list :|]| #'f2p)
   term
   )

  (term
   id
   int
   (:- term)
   (:|(| expr :|)| #'k-2-3)))


;;; The toplevel loop

(defun mread-string (s)
  (with-input-from-string (*standard-input* s)
    (parse-with-lexer #'lexer
                      *expression-parser*)))
